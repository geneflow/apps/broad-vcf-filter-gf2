Custom VCF Filter Script (Broad)
================================

Version: 0.3

This is a GeneFlow app that wraps a custom VCF filtering script from Broad.

Inputs
------

1. input: VCF file to filter.

Parameters
----------

1. min_GQ: Minimum GQ/RGQ to keep.
2. min_percent_alt_in_AD: Minimum percent alt in AD to keep for variants.
3. min_total_DP: Minimum total DP to keep. 
4. keep_GQ_0_refs: Keep ref calls with GQ/RGQ 0 despite min_GQ.
5. keep_all_ref: Don't filter reference bases.
6. output: Directory to contain the filter VCF and a stats file.


