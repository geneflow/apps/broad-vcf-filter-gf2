#!/bin/bash

# Custom VCF filter script from Broad wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => Input VCF File"
    echo "  --min_GQ => Minimum GQ"
    echo "  --min_percent_alt_in_AD => Minimum Percent Alt in AD"
    echo "  --min_total_DP => Minimum Total DP"
    echo "  --keep_GQ_0_refs => Keep GQ 0 Refs"
    echo "  --keep_all_ref => Keep All Ref Bases"
    echo "  --output => Output Directory"
    echo "  --exec_method => Execution method (singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input:,min_GQ:,min_percent_alt_in_AD:,min_total_DP:,keep_GQ_0_refs:,keep_all_ref:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
MIN_GQ="50"
MIN_PERCENT_ALT_IN_AD="0.8"
MIN_TOTAL_DP="10"
KEEP_GQ_0_REFS="yes"
KEEP_ALL_REF="yes"
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT="${input}"
            fi
            shift 2
            ;;
        --min_GQ)
            if [ -z "${min_GQ}" ]; then
                MIN_GQ=$2
            else
                MIN_GQ="${min_GQ}"
            fi
            shift 2
            ;;
        --min_percent_alt_in_AD)
            if [ -z "${min_percent_alt_in_AD}" ]; then
                MIN_PERCENT_ALT_IN_AD=$2
            else
                MIN_PERCENT_ALT_IN_AD="${min_percent_alt_in_AD}"
            fi
            shift 2
            ;;
        --min_total_DP)
            if [ -z "${min_total_DP}" ]; then
                MIN_TOTAL_DP=$2
            else
                MIN_TOTAL_DP="${min_total_DP}"
            fi
            shift 2
            ;;
        --keep_GQ_0_refs)
            if [ -z "${keep_GQ_0_refs}" ]; then
                KEEP_GQ_0_REFS=$2
            else
                KEEP_GQ_0_REFS="${keep_GQ_0_refs}"
            fi
            shift 2
            ;;
        --keep_all_ref)
            if [ -z "${keep_all_ref}" ]; then
                KEEP_ALL_REF=$2
            else
                KEEP_ALL_REF="${keep_all_ref}"
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT="${output}"
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD="${exec_method}"
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT="${exec_init}"
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Min_gq: ${MIN_GQ}"
echo "Min_percent_alt_in_ad: ${MIN_PERCENT_ALT_IN_AD}"
echo "Min_total_dp: ${MIN_TOTAL_DP}"
echo "Keep_gq_0_refs: ${KEEP_GQ_0_REFS}"
echo "Keep_all_ref: ${KEEP_ALL_REF}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "Input VCF File required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -f "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${INPUT}" ]; then
    echo "Input VCF File not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")



# MIN_GQ parameter
if [ -n "${MIN_GQ}" ]; then
    :
else
    :
fi


# MIN_PERCENT_ALT_IN_AD parameter
if [ -n "${MIN_PERCENT_ALT_IN_AD}" ]; then
    :
else
    :
fi


# MIN_TOTAL_DP parameter
if [ -n "${MIN_TOTAL_DP}" ]; then
    :
else
    :
fi


# KEEP_GQ_0_REFS parameter
if [ -n "${KEEP_GQ_0_REFS}" ]; then
    :
else
    :
fi


# KEEP_ALL_REF parameter
if [ -n "${KEEP_ALL_REF}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Directory required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    singularity)
        MNT=""; ARG=""; MNT="${MNT} -B "; MNT="${MNT}\"${SCRIPT_DIR}:/data1\""; ARG="${ARG} \"/data1/filterGatkGenotypes.py\""; ARG="${ARG} --min_GQ"; ARG="${ARG} \"${MIN_GQ}\""; if [ "${KEEP_GQ_0_REFS}" = "yes" ]; then ARG="${ARG} --keep_GQ_0_refs"; fi; ARG="${ARG} --min_percent_alt_in_AD"; ARG="${ARG} \"${MIN_PERCENT_ALT_IN_AD}\""; ARG="${ARG} --min_total_DP"; ARG="${ARG} \"${MIN_TOTAL_DP}\""; if [ "${KEEP_ALL_REF}" = "yes" ]; then ARG="${ARG} --keep_all_ref"; fi; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data7\""; ARG="${ARG} \"/data7/${INPUT_BASE}\""; CMD0="singularity -s exec ${MNT} docker://geneflow/python:2.7.18-scipy python2 ${ARG}"; CMD0="${CMD0} >\"${OUTPUT_FULL}/${OUTPUT_BASE}.vcf\""; CMD0="${CMD0} 2>\"${OUTPUT_FULL}/${OUTPUT_BASE}.stats.txt\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

